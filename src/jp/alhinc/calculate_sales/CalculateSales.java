package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String APPLICABLE_FILE_INVALID_FORMAT = "のフォーマットが不正です。";
	private static final String BRANCH_CODE_INVALID_FORMAT = "の支店コードが不正です";
	private static final String COMMODITY_CODE_INVALID_FORMAT = "の商品コードが不正です";
	private static final String SERIAL_NUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String TOTAL_AMOUNT_ERROR = "合計金額が10桁を超えました";

	// 正規表現式
	private static final String BRANCH_MATCH = "^[0-9]{3}$";
	private static final String COMMODITY_MATCH = "^[A-Za-z0-9]{8}$";
	private static final String SALES_MATCH = "^[0-9]+$";
	private static final String FILE_NAME_MATCH = "^[0-9]{8}+.rcd$";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

//		エラー処理 コマンドライン引数が一つ設定されているか否か
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コード 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, "支店定義ファイル", BRANCH_MATCH, branchNames, branchSales)) {
			return;
		}

		// 商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, "商品定義ファイル", COMMODITY_MATCH, commodityNames, commoditySales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

//		filesに入っているファイルをfor文で回し、正規表現にあったものだけをrcdFilesに格納していく。
//		（データの読み込みをしたわけではなく、ただファイルを保存しただけ）
		for(int i = 0; i < files.length; i++) {

//			エラー処理 file[i]がファイルか否か、file[i]の名前が正規表現に一致するか否か
			if(files[i].isFile() && files[i].getName().matches(FILE_NAME_MATCH)) {
				rcdFiles.add(files[i]);
			}
		}

//		エラー処理 売上ファイル名が連番か否か
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(SERIAL_NUMBER_ERROR);
				return;
			}
		}

//		rcdFilesの中に格納された売上ファイルをすべてfor文で回して、FileReaderとBufferedReaderを使いreadLineにて
//		ファイルの読み込みを行う。
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				List<String> items = new ArrayList<>();

//				1行ずつ読み込まれるため、while文を使いnullになる（ファイルの最後）まで読み込み、
//				1行ごとにitemsリストに格納する。
				while((line = br.readLine()) != null) {
					items.add(line);
				}

//				エラー処理 for文で回している売上ファイルの中身が3行か否か
				if(items.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + APPLICABLE_FILE_INVALID_FORMAT);
					return;
				}

//				エラー処理 売上ファイルの売上金額（3行目）が数字か否か
				if(!items.get(2).matches(SALES_MATCH)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

//				エラー処理 for文で回している売上ファイルの支店コード(商品コード)がbranchNames(commodityNames)に存在するか否か
				if(!branchNames.containsKey(items.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_CODE_INVALID_FORMAT);
					return;
				}

				if(!commodityNames.containsKey(items.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_CODE_INVALID_FORMAT);
					return;
				}

//				先ほど格納した売上金額（itemsの要素1←売上ファイルの2行目に金額が記述されているため）に対し
//				型変換を行い、fileSaleの中に格納する。
				Long branchSale = Long.parseLong(items.get(2));

//				branchSalesマップのキーを指定することでマップに保存されたvalueの値と上記で型変換を行った
//				売上金額（fileSale）を足して、branchSalesマップにputする。
				Long branchSaleAmount = branchSales.get(items.get(0)) + branchSale;
				Long commoditySaleAmount = commoditySales.get(items.get(1)) + branchSale;

//				エラー処理 集計した売上金額が10桁を超えたか否か
				if((branchSaleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println(TOTAL_AMOUNT_ERROR);
					return;
				}
				branchSales.put(items.get(0), branchSaleAmount);
				commoditySales.put(items.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品定義ファイル書き込み処理(メソッドの呼び出し)
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String type, String matchType, Map<String, String> Names, Map<String, Long> Sales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

//			エラー処理 支店定義ファイルもしくは商品定義ファイルが存在するか否か
			if(!file.exists()) {
				System.out.println(type + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//	エラー処理 支店定義ファイルの中身が2行か、支店コードが数字の3桁か否か
				if((items.length != 2) || (!items[0].matches(matchType))) {
					System.out.println(type + FILE_INVALID_FORMAT);
					return false;
				}

				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}